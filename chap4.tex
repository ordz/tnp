% Olivier Rodriguez - février 2014
% Document distribué sous licence CC-BY-SA 3.0 FR
% http://creativecommons.org/licenses/by-sa/3.0/fr/

\chapter{L'hypothèse de Riemann}
On rappelle que la fonction $\zeta$ définie par la formule \eqref{eq:1.0.1} est holomorphe sur le demi-plan $\{\Re(s)>1\}$.
Une des propriétés les plus frappantes et les plus importantes découvertes par Riemann est l'existence d'une équation fonctionnelle de la fonction $\zeta$ :
\begin{equation}\label{eq:5.0.1}
\zeta(s)=2^s\pi^{s-1}\sin\left(\frac12\pi s\right)\Gamma(1-s)\zeta(1-s)\quad(s\neq0,1)
\end{equation}
où $\Gamma$ désigne la fonction d'Euler initialement définie par
\[
\Gamma(s)=\frac1s\prod_{n=1}^{+\infty}\frac{(1+1/n)^s}{1+s/n}\quad(s\in\C,s\neq-n,n\in\N),
\]
plus couramment exprimée sous la forme
\begin{equation}\label{eq:5.0.2}
\Gamma(s)=\int_{0}^{+\infty}x^{s-1}e^{-x}dx\qquad(\re(s)>0).
\end{equation}
Cette fonction holomorphe dans le demi-plan $\{\re(s)>0\}$ possède un prolongement méromorphe à tout le plan complexe dont l'ensemble des pôles est $-\N$, ces pôles étant simples.\footnote{Ce prolongement est fournit par le théorème de factorisation de Weierstrass, qui permet de construire une fonction entière admettant pour zéros simples les entiers $\leq0$, en posant
\[
\frac{1}{\Gamma(z)}=e^{\gamma z}z\prod_{n=1}^{+\infty}\left(1+\frac{z}{n}\right)e^{-z/n}
\]
où $\gamma$ désigne la constante d'Euler définie dans les Notations et conventions générales.
On vérifie que la fonction $\Gamma$ ainsi obtenue coïncide bien avec la fonction définie par l'intégrale \eqref{eq:5.0.2}.} C'est la formule
\[
\zeta(s)\Gamma(s)=\int_0^{+\infty}\frac{t^{s-1}}{e^t-1}dt\quad(\re(s)>1)
\]
due à Riemann, qui permet d'établir l'équation fonctionnelle \eqref{eq:5.0.1}, dont on déduit le résultat suivant.

\begin{theo}
La fonction $\zeta$ se prolonge en une fonction méromorphe dans tout le plan complexe avec un seul pôle au point $s=1$ et ce pôle est simple.
L'équation fonctionnelle \eqref{eq:5.0.1} vaut dans tout le plan complexe.
La fonction $\zeta$ de Riemann ne s'annule pas dans le demi-plan $\{\re(s)>1\}$.
Dans le demi-plan $\{\re(s)<0\}$, ses zéros sont les points $s=-2n$ où $n\in\N^*$ ; ces zéros sont simples et sont appelés les zéros triviaux de la fonction $\zeta$.
\end{theo}

On pourra trouver une preuve des énoncés précédents
dans \cite{wagschal03b}.
On peut démontrer que la fonction $\zeta$ admet une infinité de zéros non triviaux qui sont donc situés dans la bande dite \emph{critique} $\{0\leq\re(s)\leq1\}$.
On peut dès à présent faire une remarque élémentaire concernant ces zéros : comme $\zeta(s)\in\R$ lorsque $s$ est réel $>1$, on a $\zeta(s)=\overline{\zeta}(\overline{s})$ pour $s\in\{\re(s)>1\}$, donc
pour tout $s$, la fonction $s\mapsto\overline{\zeta}(\overline{s})$ étant méromorphe dans tout le plan complexe.
Il en résulte que si $s$ est un zéro non trivial de la fonction $\zeta$, il en est de même de $\overline{s}$.
D'autre part, l'équation fonctionnelle montre que si $s$ est un zéro non trivial, alors $1-s$ est également un zéro non trivial.
Autrement dit, les zéros non triviaux sont distribués symétriquement par rapport à l'axe réel et par rapport au point $s=1/2$.\\

Nous avons vu que l'absence de zéro de $\zeta$ sur la droite $\{\re(s)=1\}$ suffit à établir le théorème des nombres premiers.
En fait, les démonstrations originales de Hadamard et de La Vallée-Poussin conduisent aussitôt à une estimation explicite du reste $\pi(x)-\frac{x}{\log x}$.
Par ailleurs, leurs démonstrations fournissent que tout zéro non trivial $\rho=\beta+i\gamma$ de $\zeta$ satisfait à
\[
\beta\leq1-\frac{c}{(\log(3+|\gamma|)^9}.
\]
pour une certaine constante $c>0$ convenable.
Dès 1898, de La Vallée-Poussin, en utilisant d'ailleurs un théorème de factorisation des fonctions analytiques dû a Hadamard, améliore ce résultat en
\[
\beta\leq1-\frac{c}{\log(3+|\gamma|)}.
\]
Ceci fournit une majoration effective pour le terme d'erreur dans le théorème des nombres premiers. 
On peut en effet déduire de l'estimation précédente que l'on a, pour une constante convenable $a>0$,
\begin{align*}
\psi(x)&=x+O(xe^{-a\sqrt{\log x}}),\\
\pi(x)&=\li(x)+O(xe^{\sqrt{\log x}}),
\end{align*}
où on rappelle que $\psi(x)=\sum_{n\leq x}\Lambda(n)$ désigne la fonction sommatoire de la fonction de von Mangoldt rencontrée à la section 3.3.
D'après la définition du logarithme intégral $\li(x)$, on a
\[
\li(x)\sim\frac{x}{\log x}\text{ lorsque }x\to+\infty,
\]
donc $\pi(x)\sim\li(x)$, mais l'approximation est en réalité de bien meilleure qualité puisque le terme d'erreur est $O(x/(\log x)^k)$ pour tout entier $k>0$.\\

Ainsi l'absence de zéro de $\zeta$ sur la droite $\{\re(s)=1\}$ fournit déjà une très bonne évaluation asymptotique de $\pi(x)$.
Dans son mémoire de 1859, Riemann émet la conjecture suivante, qui n'a toujours pas été résolue aujourd'hui, malgré les efforts conjugés de centaines de mathématiciens.

\begin{conj}[Hypothèse de Riemann]
Tous les zéros non triviaux de la fonction $\zeta$ de Riemann sont situés sur la droite critique $\{\re(s)=\frac12\}$.
\end{conj}

Cette conjecture, riche d'implications dans toutes les branches de la théorie des nombres, possède des
généralisations dans de nombreux domaines des mathématiques et se présente comme la garante de l'harmonie de la répartition des nombres premiers.\\

Les formules établies par Riemann dans son mémoire et connues sous le nom de \emph{formules explicites de la théorie des nombres}, permettent d'évaluer $\pi(x)$ en fonction des zéros non triviaux de $\zeta$ et de mieux appréhender l'incidence de l'hypothèse de Riemann sur la loi de répartition des nombres premiers.
Nous énoncerons ici des expressions analogues plus simples pour $\psi(x)$.
La justification \emph{a priori} de telles formules tient au fait que la théorie de Hadamard permet de
définir une fonction analytique essentiellement par ses zéros et ses singularités.
Dans le cas de $\zeta$, on a en effet
\[
\zeta(s)=\frac{e^{As}}{2(s-1)\Gamma(\frac12s + 1)}\prod_\rho\left(1-\frac{s}{\rho}\right)e^{s/\rho}\text{ avec }A=\log2\pi-1-\frac12\gamma
\]
où $\gamma$ désigne la constante d'Euler et où le produit porte sur tous les zéros non triviaux de $\zeta$.
Les éventuels zéros multiples sont répétés avec leur ordre de multiplicité et on peut montrer que le produit infini converge absolument.
Comme la fonction $\zeta$ définit complètement $\psi(x)$, l'existence d'un lien direct entre $\psi(x)$ et les zéros de $\zeta$ est clair.
La formule explicite est
\begin{equation}\label{eq:5.0.3}
\psi(x)=x-\sum_\rho\frac{x^\rho}{\rho}-\log2\pi-\frac12\log(1-x^2),
\end{equation}
où la somme porte sur les zéros non triviaux $\rho$ de $\zeta$.
Cette formule vaut pour $x\neq p^\nu$ avec $p$ premier, $\nu\in\N$.
Au vu d'une telle formule, il n'est guère surprenant que la taille du terme d'erreur du théorème des tombres premiers soit liée à la quantité
\[
\Theta:=\sup_{\zeta(\rho)=0}\re(s),
\]
qui satisfait à $\frac12\leq\Theta\leq1$ d'après la symétrie des zéros résultant de l'équation fonctionnelle.
On a en fait
\begin{align*}
\psi(x)&= x+O(x^\Theta(\log x)^2),\\
\pi(x)&=\li(x)+O(x^\Theta(\log x)).
\end{align*}
Réciproquement, on peut montrer que
\[
\Theta=\inf\{\vartheta\in\R\mid\pi(x)-\li(x)=O(x^\vartheta)\}.
\]
Ainsi, l'hypothèse de Riemann équivaut à la majoration
\[
\forall\varepsilon>0,\quad\pi(x)=\li(x)+O_\varepsilon(x^{1/2+\varepsilon}),
\]
soit encore
\[
\Theta = \frac12.
\]
On sait qu'un tel résultat, s'il était démontré, serait optimal.
Par ailleurs, il a été démontré que la différence $\Delta(x):=\pi(x)-\li(x)$ change de signe infiniment souvent.\\

Notons que la série $\sum_\rho x^\rho/\rho$ dans la formule \eqref{eq:5.0.3} n'est pas absolument convergente : le cas échéant, la fonction $\psi$ serait alors continue, ce qui n'est clairement pas le cas.
 Ceci suggère qu'il existe une infinité de zéros non triviaux.
 Hardy et Littlewood ont prouvé en 1914 le résultat suivant.

\begin{theo}[Hardy-Littlewood]
La fonction $\zeta$ de Riemann admet une infinité de zéros non triviaux sur la droite critique $\{\re(s)=\frac12\}. $
\end{theo}

De nombreuses approches ont été adoptées pour démontrer l'hypothèse de Riemann, mais aucune n'a abouti.
Les calculs ont établi que plusieurs milliards de zéros non triviaux de $\zeta$ sont situés sur l'axe critique.
De plus, Levinson a montré qu'au moins un tiers de ces zéros sont situés sur la droite $\{\re(s)=\frac12\}$ ; cette proportion a ensuite été améliorée jusqu'à 40\%.
Conjointement, de nombreux efforts sont également déployés pour prouver ce que l'on appelle \emph{l'hypothèse de Riemann généralisée}, liée aux séries de Dirichlet et sur laquelle repose tout un pan de la théorie analytique des nombres.\\

Soit $n$ un entier $\geq 1$, on désigne par $\left(\rquotient{\Z}{n\Z}\right)^\times$ l'ensemble des éléments inversibles de l'anneau commutatif unitaire $\rquotient{\Z}{n\Z}$.
On définit la \emph{fonction indicatrice d'Euler} $\varphi$ par
\[
\varphi(n)=\#\left(\rquotient{\Z}{n\Z}\right)^\times.
\]
Pour tout entier $n\geq1$, $\varphi(n)$ est égal au nombre des entiers qui sont premiers à $n$.
Soient $a$ et $q$ deux entiers premiers entre eux, on pose
\[
\pi(x;a,q):=\#\{p\in\mathcal{P}:p\leq x;p\equiv a\mod q\}).
\]
Dirichlet a montré en 1837 qu'il existe une infinité de nombres premiers $p\equiv a\mod q$.
Il paraît alors raisonnable de supputer une répartition équitable des nombres premiers parmi les $\varphi(q)$ classes possibles.
Compte tenu du théorème des nombres premiers, cette hypothèse conduit à la conjecture
\begin{equation}\label{eq:5.0.4}
\pi(x;a,q)\sim\frac{x}{\varphi(q)\log q}.
\end{equation}
Ce problème a été complètement résolu par de La Vallée-Poussin en 1896, grâce à des outils spécifiques forgés par Dirichlet.
Si $q$ est un entier, on appelle \emph{caractère de Dirichlet modulo $q$} une fonction arithmétique $\chi:\N\to\C$ telle que
\begin{enumerate}[(a)]
\item $\chi$ est $q$-périodique ;
\item $\chi$ est supportée par tous les entiers premiers à $q$, c'est-à-dire
\[
\chi(n)=0\Rightarrow(n,q)=1 ;
\]
\item $\chi$ est complètement multiplicative, c'est-à-dire
\[
\forall m,n\in\N,\quad\chi(mn)=\chi(m)\chi(n).
\]
\end{enumerate}
Le caractère $\chi_0$, appelé \emph{caractère principal}, est défini par
\[
\chi_0(n)=\begin{cases}
1&\text{si }(n,q)=1,\\
0&\text{sinon}.
\end{cases}
\]
On définit alors la série de Dirichlet $L(s,\chi)$ associée au caractère $\chi$ par
\[
L(s,\chi)=\sum_{n=1}^{+\infty}\frac{\chi(n)}{n^s}.
\]
On peut montrer que ces séries sont absolument convergentes dans le demi-plan $\{\re(s)>1\}$, où elles se développent en produit eulérien :
\[
L(s,\chi)=\prod_{p\in\mathcal{P}}\left(1-\frac{1}{p^s}\right)^{-1}.
\]
En particulier,
\[
L(s,\chi_0)=\prod_{\substack{p\in\mathcal{P}\\ (p,q)=1}}\left(1-\frac{1}{p^s}\right)^{-1}=\left(1-\frac{1}{q^s}\right)\zeta(s).
\]
De manière analogue à la conjecture de Gauss-Legendre, l'argument crucial qui permet d'établir la formule asymptotique \eqref{eq:5.0.4} est l'absence de zéro des fonctions $L(\cdot,\chi)$ sur la droite $\{\re(s)=1\}$.
À l'instar de la fonction $\zeta$ de Riemann, il existe une conjecture concernant la répartition des zéros de ces fonctions dans le plan complexe.

\begin{conj}[Hypothèse de Riemann généralisée]
Tous les zéros non triviaux des fonctions $L(\cdot, \chi)$ de Dirichlet sont situés sur la droite critique $\{\re(s)=\frac12\}$.
\end{conj}
Si cette conjecture était prouvée, elle fournirait l'estimation
\[
\pi(x;a,q)\sim\frac{\li(x)}{\varphi(q)}+O(\sqrt{x}\log x)\text{ lorsque }x\to+\infty,
\]
uniformément pour $x\geq3$, $q\geq1$, $(a,q)=1$.
