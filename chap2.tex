% Olivier Rodriguez - février 2014
% Document distribué sous licence CC-BY-SA 3.0 FR
% http://creativecommons.org/licenses/by-sa/3.0/fr/

\chapter{Une seconde preuve du\\ théorème des nombres premiers}
Nous exposons dans ce chapitre une preuve du théorème des nombres premiers dont l'approche originale est due à E. Landau.

\section{Convolution des fonctions arithmétiques}
On appelle \emph{fonction arithmétique} une suite à valeurs complexes $f:\N\to\C$.
On munit l'ensemble des fonctions arithmétiques de l'opération de convolution \mbox{$\star:(f,g)\mapsto f\star g$}, également appelée \emph{convolution de Dirichlet}, définie par
\[
f\star g(n)=\sum_{d|n}f(d)g\left(\frac{n}{d}\right)\quad\forall n\in\N.
\]
Cette opération correspond au produit ordinaire des séries de Dirichlet ; formellement, on a
\[
\sum_{n=1}^{+\infty}\frac{f\star g(n)}{n^s}=\sum_{n=1}^{+\infty}\frac{f(n)}{n^s}\sum_{n=1}^{+\infty}\frac{g(n)}{n^s},
\]
et on vérifie que cette égalité formelle possède un sens analytique dès que les trois séries sont absolument convergentes.

\begin{rema}
La fonction $\zeta$ de Riemann est la série de Dirichlet associée à la fonction arithmétique $\mathbf{1}$ définie par
\[
\mathbf{1}(n)=1\quad\forall n\geq1.
\]
\end{rema}

On définit la fonction $\tau$ par :
\[
\tau(n)=\mathbf{1}*\mathbf{1}(n)=\sum_{d|n}1.
\]
Autrement dit, $\tau(n)$ est le nombre de diviseurs de l'entier $n$, soit le nombre de décompositions de $n$ sous la forme $lm$ avec $l,m\in\N$.
La série de Dirichlet associée à cette fonction arithmétique est
\begin{equation}\label{eq:3.1.1}
\sum_{n=1}^{+\infty}\frac{\tau(n)}{n^s}=\sum_{n=1}^{+\infty}\frac{1}{n^s}\sum_{n=1}^{+\infty}\frac{1}{n^s}=\zeta(s)^2
\end{equation}

On vérifie que la fonction $\delta$ définie par
\[
\delta(n)=
\begin{cases}
1 & \text{si }n=1,\\
0 & \text{si }n>1
\end{cases}
\]
est un élément neutre pour la convolution et que l'ensemble des fonctions arithmétiques muni de l'addition usuelle et du produit de convolution est un anneau commutatif unitaire intègre.
On peut aussi montrer que cet anneau est factoriel.\\

On appelle \emph{fonction multiplicative} toute fonction arithmétique telle que
\[
\begin{cases}
f(1)\neq0,&\\
f(mn)=f(m)f(n) & \text{si }(m,n)=1.
\end{cases}
\]
Les fonctions multiplicatives constituent un sous-groupe du groupe des éléments inversibles pour la convolution.
L'importance cruciale des fonctions multiplicatives en théorie analytique des nombres provient essentiellement au résultat suivant.

\begin{theo}
Soit $f:\N\to\C$ une fonction multiplicative.
On a l'équivalence entre les conditions suivantes :
\begin{enumerate}[(i)]
\item la série de Dirichlet $F(s):=\sum_{n=1}^{+\infty}f(n)n^{-s}$ est absolument convergente ;
\item la série
\begin{equation}\label{eq:3.1.2}
\sum_{p\in\mathcal{P}}\sum_{\nu=1}^{+\infty}\frac{f(p^\nu)}{p^{\nu s}}
\end{equation}
est absolument convergente.
\end{enumerate}

De plus, lorsque l'une ou l'autre de ces conditions est réalisée, on a le développement en produit eulérien convergent suivant :
\[
F(s)=\prod_{p\in\mathcal{P}}\sum_{\nu=0}^{+\infty}\frac{p^\nu}{p^{\nu s}}.
\]
\end{theo}

\begin{proof}
On observe tout d'abord que la convergence de la somme double \eqref{eq:3.1.2}, qui est une conséquence immédiate de celle de $F(s)$, implique la convergence du produit infini
\[
\prod_{p\in\mathcal{P}}\left(1+\sum_{\nu=1}^{+\infty}\left|\frac{f(p^\nu)}{p^{\nu s}}\right|\right).
\]

Réciproquement, sous l'hypothèse que la série double \eqref{eq:3.1.2} converge absolument, on peut écrire, pour $x\geq1$ :
\begin{align*}
\sum_{n \leq x}\left|\frac{f(n)}{n^s}\right| &\leq \sum_{P^+(n)\leq x}\left|\frac{f(n)}{n^s}\right|\\
&= \prod_{p\leq x}\left(1+\sum_{\nu=1}^{+\infty}\left|\frac{f(p^\nu)}{p^{\nu s}}\right|\right)\\
&\leq \prod_{p\in\mathcal{P}}\left(1+\sum_{\nu=1}^{+\infty}\left|\frac{f(p^\nu)}{p^{\nu s}}\right|\right),
\end{align*}
où $P^+(n)$ désigne le plus grand facteur premier dans la décomposition de l'entier $n$.
Ceci montre bien que la série $F(s)$ est absolument convergente.
La majoration
\begin{align*}
\left|\sum_{n=1}^{+\infty}\frac{f(n)}{n^s}-\prod_{p\leq x}\sum_{\nu=0}^{+\infty}\frac{f(p^\nu)}{p^s} \right| &= \left|\sum_{P^+(n)>x}\frac{f(n)}{n^s}\right|\\
&\leq \sum_{n>x}\left|\frac{f(n)}{n^s}\right|
\end{align*}
implique alors, lorsque $x\to+\infty$, la formule du développement eulérien.
\end{proof}

Le \emph{principe de l'hyperbole}, dû à Dirichlet, est un procédé commode pour évaluer la fonction sommatoire d'un produit de convolution.

\begin{theo}[Principe de l'hyperbole]
Soient $f,g$ deux fonctions arithmétiques, de fonctions sommatoires respectives
\[
F(x):=\sum_{n\leq x}f(n)\text { et }G(x):=\sum_{n\leq x}g(n).
\]
On a pour $1\leq y\leq x$ :
\begin{equation}\label{eq:3.1.3}
\sum_{n\leq x}f\star g(n)=\sum_{n\leq y}g(n)F\left(\frac{x}{n}\right)+\sum_{m\leq x/y}f(m)G\left(\frac{x}{m}\right)-F\left(\frac{x}{y}\right)G(y).
\end{equation}
\end{theo}

\begin{proof}
Le membre de gauche de \eqref{eq:3.1.3} s'écrit encore
\begin{align*}
\sum_{md\leq x}f(m)g(d) &= \sum_{\substack{md\leq x\\ d\leq y}}f(m)g(d)+\sum_{\substack{md\leq x\\ d>y}}f(m)g(d)\\
&= \sum_{d \leq y}g(d)F\left(\frac{x}{d}\right)+\sum_{m\leq x/y}f(m)\left(G\left(\frac{x}{m}\right)-G(y)\right),
\end{align*}
ce qui implique imméditement le résultat lorsqu'on développe le dernier terme.
\end{proof}

L'application historique de cette méthode est la célèbre formule de Dirichlet :

\begin{theo}[Formule de Dirichlet]
On a
\begin{equation}\label{eq:3.1.4}
\sum_{n\leq x}\tau(n)=x(\log x+2\gamma-1)+O(\sqrt{x}).
\end{equation}
\end{theo}

\begin{proof}
On applique le principe de l'hyperbole à $y=\sqrt{x}$ et $f=g=\mathbf{1}$ (donc $F(x)=G(x)=[x]$).
Le membre de gauche de \eqref{eq:3.1.4} vaut donc
\[
2\sum_{m\leq \sqrt{x}}\left[\frac{x}{m}\right]-[\sqrt{x}]^2=2\sum_{m\leq\sqrt{x}}\frac{x}{m}-x+O(\sqrt{x}).
\]
L'identité classique
\[
\sum_{m \leq z} \frac{1}{m} = \log z + \gamma + O\left(\frac{1}{z}\right),
\]
appliquée à $z=\sqrt{x}$ permet de conclure.
\end{proof}

\section{La fonction de Möbius}
On désigne par $\mu$ la fonction de Möbius, définie comme l'inverse de convolution de la fonction $\mathbf{1}$, c'est-à-dire $\mathbf{1}\star\mu=\delta$, ou encore
\[
\sum_{d|n}\mu(d)=
\begin{cases}
1 & \text{si }n=1,\\
0 & \text{si }n>1.
\end{cases}
\]
Son existence est garantie par le fait que les fonctions multiplicatives forment un sous-groupe de l'ensemble des fonctions arithmétiques inversibles.
La multiplicativité permet, pour le calcul de cet inverse, de se restreindre aux puissances de nombres
premiers.
La relation de convolution $\mathbf{1}\star\mu=\delta$ fournit alors de proche en proche $\mu(p)=-1$ et
$\mu(p^\nu)=0$ pour $\nu\geq2$.
On a donc, pour un entier $n$ quelconque de décomposition canonique $n=\prod_{j=1}^{k}p_j^{\nu_j}$ :
\[
\mu(n)=
\begin{cases}
(-1)^k & \textrm{si }\nu_j=1,\\
 0 & \textrm{si }\max_{1\leq j\leq k}\nu_j\geq2,\quad1\leq j\leq k.
\end{cases}
\]

\begin{rema}
La relation de convolution $\delta=\mathbf{1}\star\mu$ se traduit en termes de séries de Dirichlet de la manière suivante :
\begin{align*}
1=\sum_{n=1}^{+\infty}\frac{\delta(n)}{n^s} &= \sum_{n=1}^{+\infty}\frac{\mathbf{1}\star\mu (n)}{n^s}\\
&= \sum_{n=1}^{+\infty}\frac{1}{n^s}\sum_{n=1}^{+\infty}\frac{\mu(n)}{n^s}
\end{align*}
d'où on déduit l'égalité formelle
\[
\frac{1}{\zeta(s)}=\sum_{n=1}^{+\infty}\frac{\mu(n)}{n^s}
\]
lorsque $\zeta(s)$ ne s'annule pas.
\end{rema}

On peut rassembler les propriétés algébriques de la fonction de Möbius dans l'énoncé suivant.

\begin{theo}[Inversion de Möbius]
\begin{enumerate}[(a)]
\item Soient $f,g:\N\to\C$ deux fonctions arithmétiques.
Les deux propriétés suivantes sont équivalentes :
\begin{enumerate}[(i)]
\item
\[
g(n)=\sum_{d|n}f(d)\quad\forall n\geq1,
\]
\item
\[f(n)=\sum_{d|n}g(d)\mu\left(\frac{d}{n}\right)\quad\forall n\geq 1).
\]
\end{enumerate}

\item Soient $F,G:[1;+\infty[\to\C$ des fonctions complexes.
Les deux conditions suivantes sont équivalentes :
\begin{enumerate}[(i)]
\item
\[
F(x)=\sum_{n\leq x}G\left(\frac{x}{n}\right)\quad\forall x\geq1,
\]
\item
\[
G(x)=\sum_{n\leq x}F\left(\frac{x}{n}\right)\mu(n)\quad\forall x\geq1.
\]
\end{enumerate}
\end{enumerate}
\end{theo}

\begin{proof}
\begin{enumerate}
\item Le point (a) exprime simplement l'équivalence des relations $g=f\star\mathbf{1}$ et $f=g\star\mu$.
\item On établit par exemple, l'implication (i) $\Rightarrow$ (ii), la réciproque étant analogue.
Pour tout réel $x\geq1$, on a
\begin{align*}
\sum_{n\leq x}\mu(n)F\left(\frac{x}{n}\right) &= \sum_{n\leq x}\mu(n)\sum_{m\leq x/n}G\left(\frac{x}{mn}\right)\\
&= \sum_{k\leq x}G\left(\frac{x}{k}\right)\sum_{mn=k}\mu(n).
\end{align*}
Par définition de $\mu$, la somme intérieure vaut $\delta(k)$.
Ceci implique bien (ii).
\end{enumerate}
\end{proof}

\begin{rema}
En appliquant le théorème 3.2.1 pour $G(x)\equiv1$, on obtient
\[
\sum_{n\leq x}\mu(n)\left[\frac{x}{n}\right]=1\quad\forall x\geq1,
\]
ce qui montre que
\begin{equation}\label{eq:3.2.1}
\sup_{x\in\R}\left|\sum_{n\leq x}\frac{\mu(n)}{n}\right|\leq1
\end{equation}
et suggère que $\sum_{n=0}^{+\infty}\mu(n)/n\to0$ lorsque $x\to+\infty$.
Nous verrons au paragraphe 3.4.1 que cette relation est en fait équivalente au théorème des nombres premiers.
\end{rema}

\section{La fonction de von Mangoldt et les théorèmes de Tchébychev}
C'est à Tchébychev que sont dus les premiers résultats significatifs concernant l'évaluation de $\pi(x)$.
Il utilise une forme faible de la formule de Stirling\footnote{Démontrée en 1730 : lorsque $n\to+\infty$, on a $n!\sim n^n e^{-n}\sqrt{2\pi n}$} :
\begin{equation}\label{eq:3.3.1}
\log n!=\sum_{1\leq m\leq n}\log m=n\log n-n+O(\log n)\qquad\forall n\geq2.
\end{equation}
En effet, pour $m\geq1$, on a
\begin{align*}
0\leq\int_m^{m+1}\log tdt-\log m &= \int_m^{m+1}\log\left(\frac{t}{m}\right)dt\\
&\leq \int_m^{m+1}\left(\frac{t}{m}-1\right)dt=\frac{1}{2m}.
\end{align*}
La formule \eqref{eq:3.3.1} découle directement de cette estimation, en sommant pour $m=1,2,\ldots,n-1$ et en utilisant le fait qu'une primitive de $t\mapsto\log t$ est $t\mapsto t\log t-t$.\\

L'idée de Tchébychev consiste à exploiter la décomposition de $n!$ en produit de facteurs premiers. 
Pour tout $m\geq 1$, on peut écrire :
\[
\log m=\sum_{p^\nu|m}\log p
\]
où la somme est étendue à tous les couples $(p,\nu)$ tels que $p^\nu|m$ avec $p$ premier et $\nu\geq1$.
En remplaçant dans l'expression \eqref{eq:3.3.1} et en intervertissant les sommations, il vient
\begin{align*}
\log n! &= \sum_{1\leq m\leq n}\log m\\
&= \sum_{p^\nu\leq n}\log p\sum_{\substack{1\leq m\leq n\\ p^\nu|n}}1\\
&= \sum_{p^\nu\leq n}\log p\left[\frac{n}{p^\nu}\right].
\end{align*}
On introduit alors la fonction de von Mangoldt, définie par
\[
\Lambda(d)=
\begin{cases}
\log p & \text{si }\exists\nu\geq1:d=p^\nu,\\
0 & \text{sinon}.
\end{cases}
\]
On vérifie que cette fonction satisfait à la relation de convolution $\Lambda\star\mathbf{1}=\log$.
En convolant par la fonction de Möbius $\mu$, on obtient $\Lambda=\mu\star\log$.
On peut également vérifier que
$\Lambda=-(\mu\log)\star\mathbf{1}$.

On déduit alors de ce qui précède la formule asymptotique
\begin{equation}\label{eq:3.3.2}
\sum_{d\leq n}\Lambda(d)\left[\frac{n}{d}\right]=n\log n-n+O(\log n)\quad\forall n\geq2.
\end{equation}
Notons $B(n)$ le membre de gauche et posons $B(x)=B([x])$ pour tout $x>0$.
On considère la fonction sommatoire de $\Lambda$,
\[
\psi(x)=\sum_{d\leq x}\Lambda(d).
\]
On souhaite établir un encadrement de $\psi(x)$ grâce à l'estimation de $B(x)$ : on verra plus loin que cela permettra d'obtenir un encadrement de même qualité pour $\pi(x)$.

\begin{theo}[Tchébychev]
On a, pour tout réel $x\geq 2$ :
\[
x\log2+O(\log x)\leq\psi(x)\leq x\log4+O\big(\log x)^2\big).
\]
\end{theo}

\begin{proof}
On pose
\[
\chi(u) = [u] - 2 \left[\frac{u}{2}\right]\ \qquad (u > 0).
\]
Alors $\chi$ est une fonction 2-périodique vérifiant
\begin{equation}\label{eq:3.3.3}
\chi(u)=
\begin{cases}
0 & \textrm{si }0\leq u<1,\\
1 & \textrm{si }1\leq u<2.
\end{cases}
\end{equation}
On peut évaluer de deux manières la quantité
\[
B_2(x):=B(x)-2B\left(\frac{x}{2}\right).
\]
D'une part, on a d'après la formule \eqref{eq:3.3.2} :
\begin{align*}
B_2(x) &= x\log x-x-2\left(\frac12 x\log\left(\frac12 x\right)-\frac12 x\right)+O(\log x)\\
&= x\log2+O(\log x),
\end{align*}
et d'autre part :
\[
B_2(x)=\sum_{d\leq x}\Lambda(d)\chi\left(\frac{x}{d}\right).
\]
Compte tenu de la propriété \eqref{eq:3.3.3} de $\chi$, on déduit de cette dernière identité :
\[
\psi(x)-\psi\left(\frac{x}{2}\right)\leq B_2(x)\leq\psi(x)
\]
d'où la minoration de $\psi(x)$ :
\[
\psi(x)\geq x\log2+O(\log x)\quad\forall x\geq2.
\]
En utilisant la minoration de $B_2(x)$ de manière inductive, on obtient
\begin{align*}
\psi(x)\leq B_2(x)+\psi\left(\frac{x}{2}\right) &\leq B_2(x)+B_2\left(\frac{x}{2}\right)+\psi\left(\frac{x}{4}\right)\\
&\leq \ldots\leq\sum_{0\leq j\leq k}B_2\left(\frac{x}{2^j}\right)+\psi\left(\frac{x}{2^{k + 1}}\right)
\end{align*}
où $k$ est un entier arbitraire.
On choisit
\[
k=K(x):=\left[\frac{\log x}{\log 2}\right]
\]
de sorte que $\psi\left(x/2^{k + 1}\right) = 0$.
Il vient
\begin{align*}
\psi(x) &\leq \sum_{0\leq j\leq K(x)}\left(\frac{x\log 2}{2^j}+O(\log x)\right)\\
&\leq 2x\log2+O((\log x)^2).
\end{align*}
Ceci achève la démonstration.
\end{proof}

Comme annoncé, on peut alors déduire de ce résultat une information comparable relative à $\pi(x)$.

\begin{coro}
Lorsque $x\to\infty$, on a :
\[
\big(\log2+o(1)\big)\frac{x}{\log x}\leq\pi(x)\leq\big(\log4+o(1)\big)\frac{x}{\log x}.
\]
\end{coro}

\begin{proof}
On a $\psi(x)=\sum_{p^\nu\leq x}\log p$, où la somme porte sur tous les couples $(p,\nu)$ avec $p$ premier et $\nu\geq1$.
Pour chaque $p$ fixé, l'entier $\nu$ peut prendre exactement $[\log x/\log p]$ valeurs, donc
\[
\psi(x)=\sum_{p\leq x}\left[\frac{\log x}{\log p}\right]\log p.
\]
Grâce à l'encadrement $[u]\leq u\leq[u]+1$ pour $u\geq1$, il suit d'abord
\[
\psi(x)\leq\pi(x)\log x\leq2\psi(x),
\]
puis, en utilisant la majoration du théorème 3.3.1,
\begin{align*}
\pi(x)\log x-\psi(x) &= \sum_{p\leq x}\left(\log x-\left[\frac{\log x}{\log p}\right]\log p\right)\\
& \leq \sum_{p\leq\sqrt{x}}\log p+\sum_{\sqrt{x}<p\leq x}\log\left(\frac{x}{p}\right)\\
& \leq \psi(\sqrt{x})+\sum_{\sqrt{x}<p\leq x}\int_{x}^{p}\frac{dt}{t} \\
&= O(\sqrt{x})+\int_{\sqrt{x}}^x\frac{\pi(t)}{t}dt=O\left(\frac{x}{\log x}\right)
\end{align*}
d'où
\[
\pi(x)=\frac{\psi(x)}{\log x}+O\left(\frac{x}{(\log x)^2}\right).
\]
Le théorème 3.3.1 permet de conclure.
\end{proof}

\begin{rema}
Une autre conséquence du travail de Tchébychev, indiquée par lui-même, est l'encadrement
\[
\liminf_{x \to+\infty}\frac{\pi(x)}{x/\log x}\leq1\leq\limsup_{x\to+\infty}\frac{\pi(x)}{x/\log x},
\]
autrement dit : si le rapport $\pi(x)\log x/x$ admet une limite lorsque $x\to+\infty$, cette limite
est $1$.
\end{rema}

\section{La démonstration de Newman revisitée}
Commençons par établir le résultat élémentaire suivant :

\begin{lemm}
Soit ${(a_n)}_{n\,\in\,\Bbb{N}}$ une suite de nombres complexes satisfaisant à
\[
\sum_{n \leq t}a_n=E(t)+O\big(R(t)\big),\quad1\leq t\leq x
\]
où $E$ est une fonction de classe $\mathcal{C}^1$ sur $[1;x]$ et $R:[1\,;\,x]\to\R_+$ est une fonction monotone.
Alors, pour toute fonction $b:[1;x]\to\C$ continue et de classe $\mathcal{C}^1$ par morceaux, on a
\[
\sum_{n\leq x}a_n b(n)=\int_1^xE'(t)b(t)dt+O\big(R_1(x)\big)
\]
avec
\[
R_1(x)=|E(1)b(1)|+|R(x)b(x)|+\int_1^x|R(t)b'(t)|dt.
\]
\end{lemm}

\begin{proof}
On pose $A(t)=\sum_{n\leq t}a_n$.
Le membre de gauche vaut
\[
b(x)A(x)-\sum_{n \leq x}a_n\int_n^xb'(t)dt=b(x)A(x)-\int_1^xA(t)b'(t)dt.
\]
Le résultat indiqué en découle immédiatement en remplaçant dans la dernière intégrale $A(t)$ par $E(t)+O\big(R(t)\big)$ et en intégrant par parties la contribution du terme principal $E(t)$.
\end{proof}

\subsection{Le théorème de Landau}
Nous avons vu que la fonction de von Mangoldt $\Lambda=\mu\star\log$ est fortement liée à la fonction caractéristique des nombres premiers et possède une fonction sommatoire
\[
\psi(x)=\sum_{n\leq x}\Lambda(n)
\]
qui, d'après le corollaire 3.3.2, vérifie
\begin{equation}\label{eq:3.4.1}
\psi(x)\sim\pi(x)\log(x)\text{ lorsque }x\to+\infty.
\end{equation}
Il est donc naturel de se demander si la valeur moyenne de la fonction de Möbius $\mu(n)$ possède une interprétation simple dans le cadre de l'étude asymptotique des fonctions $\pi(x)$ et $\psi(x)$.
Le théorème suivant, établi par Landau en 1909, répond à cette question.

\begin{theo}[Landau]
On a l'équivalence entre les assertions suivantes :
\begin{enumerate}[(i)]
\item
\[
\psi(x)\sim x\text{ lorsque }x\to+\infty,
\]
\item
\[
\sum_{n\leq x}\mu(n)=o(x)\text{ lorsque }x\to+\infty,
\]
\item
\[
\sum_{n=1}^{+\infty}\frac{\mu(n)}{n}=0.
\]
\end{enumerate}
\end{theo}

\begin{rema}
L'assertion (i) est équivalente au théorème des nombres premiers, c'est donc l'implication (ii) $\Rightarrow$ (i) qui sera utilisée dans sa démonstration.
\end{rema}

\begin{proof}[Démonstration du théorème de Landau]
\begin{enumerate}
\item (iii) $\Rightarrow$ (ii). Supposons que
\[
m(x):=\sum_{n\leq x}\frac{\mu(n)}{n}=o(1)\text{ lorsque }x\to+\infty.
\]
En appliquant le lemme 3.4.1 à $E(t)=0$ et $R(t)=\sup_{y\geq t}|m(y)|$, on obtient
\[
\sum_{n\leq x}\mu(n)\ll xR(x)+\int_1^xR(t)dt=o(x).
\]

\item (ii) $\Rightarrow$ (i). Notons
\[
M(x)=\sum_{n\leq x}\mu (n).
\]
On établit d'abord une identité de convolution pour la fonction $\Lambda-\mathbf{1}$, dont on doit montrer qu'elle possède une fonction sommatoire qui est $o(x)$.
On a
\begin{align*}
\Lambda-\mathbf{1}=(\log-\tau)\star\mu &= (\log-\tau+2\gamma\mathbf{1})\star\mu-2\gamma\delta\\
&= f\star\mu-2\gamma\delta,
\end{align*}
où la fonction $f:=\log-\tau+2\gamma\mathbf{1}$ satisfait à
\begin{equation}\label{eq:3.4.2}
F(x):=\sum_{n\leq x}f(n)=O(\sqrt{x}),
\end{equation}
d'après la formule de Dirichlet \eqref{eq:3.1.4} et l'identité classique
\[
\sum_{n\leq x}\log n=x\log x-x+O(1+\log x)\quad\forall x\geq1.
\]
On va montrer que
\[
H(x):=\sum_{n\leq x}f\star\mu(n)=o(x),
\]
en utilisant l'estimation \eqref{eq:3.4.2} et le principe de l'hyperbole (théorème 3.1.2).
Soit $y>2$ fixé, on peut écrire en effet :
\[
H(x)=\sum_{n\leq x/y}\mu(n)F\left(\frac{x}{n}\right)+\sum_{m\leq y}f(m)M\left(\frac{x}{m}\right)-F(y)M\left(\frac{x}{y}\right).
\]
Sous l'hypothèse (ii), il suit donc, pour $y$ fixé,
\begin{align*}
\limsup_{x\to+\infty} \left|\frac{H(x)}{x}\right| &\leq \limsup_{x\to+\infty}\frac1x\sum_{n\leq x/y}\left|F\left(\frac{x}{n}\right)\right|\\
& \ll \limsup_{x\to+\infty}\frac1x\sum_{n\leq x/y}\sqrt{\frac{x}{n}}=O\left(\frac{1}{\sqrt{y}}\right).
\end{align*}
Comme $y$ peut être choisi arbitrairement grand, ceci implique bien $H(x)=o(x).$

\item (i) $\Rightarrow$ (iii). Il sera fait usage de la majoration
\[
\left|\sum_{n\leq x}\frac{\mu(n)}{n}\right|\leq1
\]
prouvée à la section 3.2.
On remarque d'abord que la formule d'inversion de Möbius $\mu\star\mathbf{1}=\delta$ implique
\[
\sum_{md=x}\frac{\mu(m)}{md}=\delta(n),
\]
d'où, par sommation pour $n\leq x$,
\[
1=\sum_{m\leq x}\frac{\mu(m)}{m}\sum_{d\leq x/m}\frac1d\quad\forall x\geq1.
\]
L'évaluation de la somme intérieure donne
\begin{align*}
1 &= \sum_{m\leq x}\frac{\mu(m)}{m}\left(\log\left(\frac{x}{m}\right)+\gamma+O\left(\frac{m}{x}\right)\right)\\
&= m(x)(\log x+\gamma)-G(x)+O(1),
\end{align*}
où l'on a posé
\[
G(x):=\sum_{m\leq x}\frac{\mu(m)\log(m)}{m}.
\]
Il suffit donc d'établir que l'on a
\[
G(x)=o(\log x)\text{ lorsque }x\to+\infty.
\]
À cette fin, on vérifie la relation de convolution
\[
\mu\log=-\Lambda\star\mu=(\mathbf{1}-\Lambda)\star\mu-\delta.
\]
Posant
\[
P(x):=[x]-\psi(x)=\sum_{n\leq x}\big(\mathbf{1}(n)-\Lambda(n)\big)=o(x),
\]
on peut donc écrire
\begin{align*}
G(x)+1 &= \sum_{j\leq x}\left(\frac{P(j)-P(j-1)}{j}\right)m\left(\frac{x}{j}\right)\\
&= \sum_{j\leq x}P(j)\left(\frac{m(x/j)}{j}-\frac{m(x/(j+1))}{j+1}\right)+o(1)\\
&= \sum_{j\leq x}\frac{P(j)}{j}\left\{m_j(x)+\frac{1}{j+1}m\left(\frac{x}{j+1}\right)\right\}+o(1),
\end{align*}
avec $m_j(x):=m(x/j)-m(x/j+1)$.
On se donne alors, pour chaque $\varepsilon>0$, un nombre entier $j_0(\varepsilon)$ tel que $|P(j)|\leq\varepsilon j$ pour $j>j_0(\varepsilon)$.
En utilisant la majoration \eqref{eq:3.2.1} et l'inégalité
\[
|m_j(x)|\leq\sum_{x/(j+1)<n\leq x/j}\frac1n,
\]
il vient
\[
|G(x+1)|\leq3\sum_{j\leq j_0(\varepsilon)}\frac{|P(j)|}{j}+2\varepsilon\sum_{n\leq x}\frac1n+o(1),
\]
d'où $\limsup_{x\to+infty}|G(x)|/\log x\leq2\varepsilon$.
Comme on peut choisir $\varepsilon$ arbitrairement petit, cela montre bien que
\[
G(x)=o(\log x)
\]
ce qui achève la démonstration du théorème.
\end{enumerate}
\end{proof}

\subsection{Fin de la preuve de Newman}
La preuve du théorème des nombres premiers telle qu'elle est présentée dans \cite{newman98} nécessite d'établir au préalable l'énoncé qui suit.
Ce résultat a été établi par Ingham par des méthodes d'Analyse de Fourier.
La démonstration que nous proposons est due à Newman et repose sur l'utilisation de l'intégrale curviligne
\[
\int_{\Gamma}F(z)N^z\left(\frac{1}{z}+\frac{z}{R^2}\right)dz,
\]
où nous préciserons le lacet $\Gamma$.
Nous achèverons la démonstration du théorème de Hadamard et de La Vallée-Poussin par
le corollaire de Landau, qui établit l'assertion (ii) du théorème 3.4.2.
Au vu de ce résultat, le Théorème des Nombres Premiers s'en déduira aussitôt.

\begin{theo}[Ingham]
Soit $(a_n)$ une suite de nombres complexes tels que $|a_n|\leq1$, on considère la série de Dirichlet
\[
F(s):=\sum_{n=1}^{+\infty}\frac{a_n}{n^s}.
\]
Cette série converge dans le demi-plan ouvert $\{\re(s)>1\}$ et y définit une fonction analytique.
Si, en fait, $F(s)$ est analytique dans un voisinage du demi-plan fermé $\{\re(s)\geq1\}$, alors la série de Dirichlet $\sum_{n=1}^{+\infty}a_nn^{-s}$ converge dans un voisinage du demi-plan fermé $\{\re(s)\geq1\}$.
\end{theo}

\begin{proof}
Supposons $F$ analytique dans le demi-plan fermé $\{re(s)\geq1\}$.
On fixe $w$ de telle sorte que $\re(w)\geq1$ : ainsi, la fonction $\left(z \mapsto F(z+w)\right)$ est analytique dans le demi-plan fermé $\{\re(z)\geq0\}$.
Soient
\begin{align*}
R &\geq1,\\
\delta &= \delta(R)\text{ tel que }0<\delta\leq\frac12,
M &= M(R)\geq0.
\end{align*}
de sorte que sur $\{-\delta\leq\re(z);|z|\geq R\}$, on ait
\begin{align*}
z &\mapsto F(z+w)\text{ est analytique},
|F(z+w)| &\leq M.
\end{align*}
On introduit à présent le lacet $\Gamma$ (orienté dans le sens direct) constitué
\begin{align*}
&\text{de l'arc de cercle }\{|z|=R,\re(z)>-\delta\}\\
\text{et }&\text{du segment vertical }\{\re(z)=-\delta;|z|\leq R\}
\end{align*}
On désigne par $\Gamma_A$ et $\Gamma_B$ les morceaux de $\Gamma$ contenus respectivement dans les demi-plans droit et gauche.
D'après le théorème de Cauchy, on a
\[
2\pi iF(w)=\int_\Gamma F(z+w)N^z\left(\frac1z+\frac{z}{R^2}\right)dz.
\]
avec $n\in\N$.
Sur $\Gamma_A$, on a
\[
F(z+w)=\sum_{n=1}^{+\infty}\frac{a_n}{n^{z+w}}=\sum_{n=1}^N\frac{a_n}{n^{z+w}}+\sum_{n=N+1}^{+\infty}\frac{a_n}{n^{z+w}}
\]
et on pose
\begin{align*}
S_N(z+w) &= \sum_{n=1}^N\frac{a_n}{n^{z+w}}\\
r_N(z+w) &= \sum_{n=N+1}^{+\infty}\frac{a_n}{n^{z+w}}
\end{align*}
Une nouvelle application du théorème de Cauchy donne
\[
\int_{\Gamma_A}S_N(z+w)N^z\left(\frac1z+\frac{z}{R^2}\right)dz=2\pi iS_N(w)-\int_{-\Gamma_A}S_N(z+w)N^z \left(\frac1z+\frac{z}{R^2}\right)dz,
\]
où $-\Gamma_A$ désigne l'image de $\Gamma_A$ par la symétrie par rapport à l'origine.
Ainsi, en effectuant le changement de variable $z \mapsto-z$, on a
\[
\int_{\Gamma_A}S_N(z+w)N^z\left(\frac1z+\frac{z}{R^2}\right)dz=2\pi iS_N(w)-\int_{\Gamma_A}S_N(w-z)N^{-z}\left(\frac1z+\frac{z}{R^2}\right)dz.
\]
On obtient alors
\[
2\pi i[F(w)-S_n(w)]=\int_{\Gamma_A}\left[r_N(z+w)N^z-\frac{S_N(w-z}{N^z}\right]\left(\frac{1}{z}+\frac{z}{R^2}\right)dz+\int_{\Gamma_B}F(z+w)N^z\left(\frac1z+\frac{z}{R^2}\right)dz.
\]
Notons $x=\re(z)$, on a
\begin{align*}
\frac1z+\frac{z}{R^2}=\frac{2x}{R^2} &\text{ sur le cercle }\{|z|=R\},\text{ en particulier sur }\Gamma_A,\\
\left|\frac1z+\frac{z}{R^2}\right|\leq\frac1\delta\left(1+\frac{|z|^2}{R^2}\right)\leq\frac{2}{\delta} &\text{ sur le segment vertical }\{re(z)=-\delta;|z|\leq R\},
\end{align*}
puis
\begin{align*}
|r_N(z+w)|=\left|\sum_{n=N+1}^{+\infty}\frac{1}{n^{z+w}}\right|\leq\sum_{n=N+1}^{+\infty}\left|\frac{1}{n^{z+w}}\right| &\leq \sum_{n=N+1}^{+\infty}\frac{1}{n^{x+1}}\\
&\leq \int_{N}^{+\infty}\frac{dt}{t^{x+1}}=\frac{1}{xN^x},\\
\end{align*}
et
\begin{align*}
|S_n(w-z)| =\left|\sum_{n=1}^{+\infty}\frac{1}{n^{w-z}}\right|\leq \sum_{n=1}^{+\infty}\left|\frac{1}{n^{w-z}}\right| &\leq \sum_{n=1}^N n^{x-1}\\
&\leq N^{x-1}+\int_{0}^N t^{x-1}dt=N^x\left(\frac1N+\frac1x\right).
\end{align*}
On a alors sur $\Gamma_A$ :
\[
\left|\left(r_N(z+w)N^z-\frac{S_N(w-z)}{N^z}\right)\left(\frac1z+\frac{z}{R^2}\right)\right|\leq\left(\frac1x+\frac1x+\frac1N\right)+\frac{2x}{R^2}\leq\frac{4}{R^2}+\frac{2}{RN}
\]
d'où
\[
\int_{\Gamma_A}\left|\left(r_N(z+w)N^z-\frac{S_N(w-z)}{N^z}\right)\left(\frac{1}{z}+\frac{z}{R^2}\right)\right|dz\leq\frac{4\pi}{R}+\frac{2\pi}{N}.
\]
On obtient alors
\begin{align*}
\int_{\Gamma_B}\left|F(z+w)N^z\left(\frac1z+\frac{z}{R^2}\right)\right|dz &\leq \int_{-R}^{R}MN^{-\delta}\frac2\delta dy+2M\int_{-\delta}^0n^x\frac{2|x|}{R^2}\frac{3}{2}dx\\
&\leq\frac{4MR}{\delta N^\delta}+\frac{6M}{R^2(\log N)^2}.
\end{align*}
Enfin, il vient
\[
|F(w)-S_N(w)|\leq\frac{2}{R}+\frac1N+\frac{MR}{\delta N^\delta}+\frac{M}{R^2(\log N)^2}<\varepsilon
\]
pour $N$ suffisamment grand, si on fixe $R=3/\varepsilon$.
Ceci achève la démonstration.
\end{proof}

Il s'ensuit l'énoncé suivant.

\begin{coro}[Landau]
On a
\[
\sum_{n\leq x}\mu(n)=o(x)\text{ lorsque }x\to+infty),
\]
où $\mu$ désigne la fonction de Möbius.
\end{coro}

\begin{proof}
On a effectivement $|\mu(n)|\leq1$ pour tout $n\in\N$ et on a (de manière formelle)
\[
\sum_{n=1}^{\infty} \frac{\mu(n)}{n^s} = \frac{1}{\zeta(s)}
\]
comme il a été observé dans la section 3.2.
Le théorème 3.4.3 fournit immédiatement le résultat souhaité.
\end{proof}

\begin{proof}[Fin de la démonstration du théorème des nombres premiers]
Compte tenu de l'implication (ii) $\Rightarrow$ (i) dans le théorème de Landau 3.4.2, on a alors
\[
\psi(x)\sim x\text{ lorsque }x\to+\infty.
\]
Grâce à l'équivalent \eqref{eq:3.4.1}
\[
\psi(x)\sim\pi(x)\log x\text{ lorsque }x\to+\infty,
\]
on a finalement
\[
x\sim\pi(x)\log x\text{ lorsque }x\to+\infty,
\]
ce qui prouve le Théorème des Nombres Premiers.
\end{proof}
