% Olivier Rodriguez - février 2014
% Document distribué sous licence CC-BY-SA 3.0 FR
% http://creativecommons.org/licenses/by-sa/3.0/fr/

\chapter*{Annexe : Factorisation des\\ fonctions analytiques}
\addcontentsline{toc}{chapter}{Annexe : factorisation des fonctions analytiques}
Les résultats que nous mentionnons ci-après sont présentés de façon détaillée et démontrés dans l'ouvrage \cite{wagschal03b}.
Nous nous bornerons ici à en donner les énoncés.\\

On définit les facteurs primaires de Weierstrass comme suit :
\begin{align*}
E_0(z)&=1-z\\
E_k(z)&=(1-z)\exp\left(z+\frac{z^2}{2}+\ldots+\frac{z^k}{k}\right)\text{ pour }\geq1.
\end{align*}

\begin{theo}[Théorème de factorisation de Weierstrass]
Soient $(a_n)_{n\geq1}$ une suite de nombres complexes telle que $a_n\neq0$ et $\lim_{|a_n|\to+\infty}=+\infty$, $k(n)\in\N$ des entiers tels que
\[
\sum_{n=1}^{+\infty}\left(\frac{r}{|a_n|}\right)^{k(n)+1}<\infty\quad(r>0)
\]
et soit $g:\C\to\C$ une fonction entière, alors la fonction
\begin{equation}\label{eq:A.0.1}
f(z)=e^{g(z)}\prod_{n=1}^{+\infty}E_{k(n)}\left(\frac{z}{a_n}\right),
\end{equation}
où le produit infini est normalement convergent sur tout compact de $\C$, est une fonction entière dont
$\bigcup_{n=1}^{+\infty}\{a_n\}$ est l'ensemble des zéros et
\begin{equation}\label{eq:A.0.2}
\omega(f;a_n):=\#\{p\in\N^\times:a_p=a_n\}
\end{equation}
Réciproquement, toute fonction entière dont l'ensemble des zéros est $\bigcup_{n=1}^{+\infty}\{a_n\}$ avec la multiplicité donnée par \eqref{eq:A.0.1} est de la forme \eqref{eq:A.0.2}.
\end{theo}

Soit $f:\C\to\C$ une fonction entière, pour tout $r>0$ on pose
\[
M(r)=\max_{|z|=r}|f(z)|.
\]
La fonction $M:\R_+\to\R_+$ est croissante d'après le principe du maximum et même strictement croissante si la fonction $f$ n'est pas constante.
On définit l'ordre de $f$ par
\[
\rho=\inf\{A\geq0:(\exists r_0\geq0)(\forall r\geq r_0)\big(M(r)\leq e^{r^A}\big)\}\in[0;+\infty[.
\]

On notera $\ord(f)$ l'ordre de $f$.
On dit que $f$ est d'ordre fini si $\rho$ est fini.\\

Il existe des relations étroites reliant l'ordre d'une fonction entière et la répartition de ses zéros dans le plan complexe.
Soit $f$ une fonction entière non identiquement nulle, on notera $(a_n)_{n\geq1}$ la suite de ses zéros non nuls, chaque zéro étant répété un nombre de fois égal à sa multiplicité.
Si $r_n=|a_n|$, on peut supposer la suite $(r_n)$ croissante, soit
\[
0<r_n=|a_n|,r_n\leq r_{n+1}.
\]
La suite $(a_n)$ peut être finie ; lorsqu'elle est finie, on a $r_n\to+\infty$.
On note $k$ le plus petit entier, s'il existe, tel que
\[
\sum_{n=1}^{+\infty}r_n^{-(k+1)}<\infty.
\]
Lorsqu'un tel entier n'existe pas, on pose $k=\infty$ ; $k\in\N\cup\{\infty\}$ est appelé le \emph{rang} de la suite $(a_n)$.
Le théorème de factorisation de Weierstrass permet d'écrire
\[
f(z)=e^{Q(z)}z^m P(z)
\]
où $m=\omega(f;0)$,
\[
P(z)=\prod_{n=1}^{+\infty}E_k\left(\frac{z}{a_n}\right)
\]
et $Q$ est une fonction entière qui n'est définie qu'à l'addition près d'un multiple de $2\pi i$.
L'\emph{exposant de convergence} est une notion qui complète celle de rang.
On le définit comme suit :
\[
\rho_c=\inf\{\rho\geq0:\sum_{n=1}^{+\infty}r_n^{-\rho}<\infty\}\in[0;+\infty[
\]
où l'on convient que $\inf\emptyset=\infty$.
On dispose alors du résultat suivant.

\begin{theo}[Théorème de factorisation de Hadamard]
Soit $f$ une fonction entière d'ordre fini $\rho$ non identiquement nulle, alors la suite $(a_n)$ de ses zéros non nuls est de rang fini $k$ et
\[
f(z)=e^{Q(z)}z^m\prod_{n=1}^{+\infty}E_k\left(\frac{z}{a_n}\right),\quad m=\omega(f;0),
\]
où $Q$ est un polynôme de degré $q$ et
\[
\rho = \max(q,\rho_c).\]
\end{theo}

\begin{rema}
Le théorème de Hadamard se révèle être un outil particulièrement puissant pour résoudre le problème de la détermination du terme exponentiel dans le théorème de Weierstrass.
\end{rema}

La preuve de l'existence d'une infinité de zéros non triviaux de la fonction $\zeta$ repose sur l'énoncé suivant.

\begin{prop}
Soit $f$ une fonction entière d'ordre fini $\rho\in \R\setminus\N$.
Alors, pour tout $a\in\C$, la fonction $z\mapsto f(z)-a$ admet une infinité dénombrable de zéros dont l'exposant de convergence est $\rho$.
\end{prop}
