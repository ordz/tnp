% Olivier Rodriguez - février 2014
% Document distribué sous licence CC-BY-SA 3.0 FR
% http://creativecommons.org/licenses/by-sa/3.0/fr/

\chapter{Démonstrations, variantes et conséquences du théorème\\ des nombres premiers}
Les démonstrations originales de Hadamard et de La Vallée-Poussin prenaient comme point de départ l'expression de la fonction $\zeta$ comme produit eulérien.
Ils établissaient la non-annulation de $\zeta$ sur la droite $\{\re(s)=1\}$ par des arguments directs, superficiellement différents mais au fond de même nature : ils montraient en effet que si $\zeta$ admettait un zéro en $1+it_0$ où $t_0\in\R_+^*$, alors elle posséderait un pôle en $1+it_0$ -- ce qui n'est pas le cas.
Il paraît difficile de présenter cet argument de façon plus suggestive que ne le fait Hadamard dans la note \cite{hadamard68}, présentée à l'Académie des Sciences de Paris le 22 juin 1896, où il annonce sa démonstration du théorème des nombres premiers.
La non-annulation de $\zeta$ sur $\{\re(s)=1\}$ est établie sous une forme qualitative plus précise, spécifiant un voisinage de $\{\re(s)=1\}$ où $\zeta$ ne s'annule pas.
Hadamard et de La Vallée-Poussin concluaient leurs démonstrations en exprimant $\pi(x)$ en fonction de $\zeta_\mathcal{P}$ ou, ce qui revient à peu près au même, de $\log\zeta$, grâce à une utilisation ingénieuse de la formule des résidus.\\

Vers 1930, Wiener obtint une nouvelle démonstration du théorème des nombres premiers, où les techniques d'analyse complexe sont remplacées dans une large mesure par l'analyse harmonique de la fonction $t\mapsto\zeta(1+i t)$.
Cette philosophie est menée à son terme dans la démonstration de Kahane que nous avons présentée en premier lieu.\\

Mentionnons enfin qu'en 1949, Erdös \cite{erdos49} et Selberg \cite{selberg49} ont donné\footnote{La
démonstration fut établie conjointement par Erdös et Selberg, mais
ce dernier seul obtint la médaille Fields.} une preuve élémentairedu théorème des nombres premiers, c'est-à-dire une preuve qui évite tout recours à la théorie des fonctions analytiques et n'utilise que des inégalités d'analyse réelle élémentaire.
L'outil essentiel est ici la formule asymptotique suivante, aujourd'hui connue sous le nom d'identité de Selberg :
\[
\sum_{\substack{p\in\mathcal{P}\\ p\leq x}}(\log p)^2+\sum_{\substack{p,q\in\mathcal{P}\\ pq\leq x}}\log p\log q=2x\log x+O(x).
\]
La procédure est semblable à celle employée à la section 3.3 pour obtenir l'encadrement du théorème de Tchébychev 3.3.1 à partir d'une forme faible de la formule de Stirling.
L'introduction de l'exposant $2$ permet ici d'obtenir immédiatement une formule asymptotique.
En utilisant l'identité de Selberg, Erdös a notamment prouvé que le rapport $p_{n+1}/p_n$ de deux nombres premiers consécutifs tend vers $1$.

\section{Reformulations et conséquences du\\théorème des nombres premiers}
Le théorème des nombres premiers admet en fait bien des reformulations, dont l'équivalence découle de raisonnements élémentaires.
Citons deux d'entre elles :

\begin{theo}
Chacune des assertions suivantes est équivalente au théorème des nombres premiers 1.0.2 :
\begin{enumerate}[(i)]
\item Lorsque l'entier $n\to\infty$, on a
\begin{equation}\label{eq:4.1.1}
p_n\sim n\log n,
\end{equation}
où $p_n$ désigne le $n$-ième nombre premier.
\item Lorsque $x\to\infty$, on a
\begin{equation}\label{eq:4.1.2}
\sum_{\substack{p\in \mathcal{P}\\ p\leq x}}\log p\sim x.
\end{equation}
\end{enumerate}
\end{theo}

\begin{proof}
\begin{enumerate}
\item \eqref{eq:1.0.2} $\Longrightarrow$ \eqref{eq:4.1.1}.
Il s'ensuit du théorème des nombres premiers 1.0.2 :
\[
\log\pi(k)\sim\log k\text{ lorsque }k\to+\infty),
\]
puis
\[
\pi(k)\log\pi(k)\sim k.
\]
En faisant $k=p_n$, on obtient l'équivalent \eqref{eq:4.1.1}.

\item \eqref{eq:4.1.1} $\Rightarrow$ (1.0.2).
Pour tout $x\in[2;+\infty[$, on a :
\[
p_\pi(x)\leq x<p_{\pi(x)+1}.
\]
D'après \eqref{eq:4.1.1}, on a :
\[
p_\pi(x)\sim\pi(x)\log\pi(x)
\]
et
\[
p_{\pi(x)+1}\sim(\pi(x)+1)\log(\pi(x)+1)\sim\pi(x)\log\pi(x).
\]
Par conséquent :
\[
x\sim\pi(x)\log\pi(x)
\]
ce qui implique (comme plus haut) que
\[
\log x \sim \log \pi(x),
\]
d'où enfin
\[
\pi(x)\sim\frac{x}{\log x}.
\]

\item \eqref{eq:1.0.2} $\Rightarrow$ \eqref{eq:4.1.2}.
On va utiliser la formule de sommation suivante, qui servira à nouveau dans la prochaine section :

\begin{lemm}
Pour toute suite de nombres complexes ${(a_n)}_{n\,\in\,\Bbb{N}^*}$ et tout $N \in \Bbb{N}^*$, on a
\begin{equation}\label{eq:4.1.3}
\sum_{\substack{p\in \mathcal{P}\\ p\leq N}}a_p=\pi(N)a_n+\sum_{n=1}^{N-1}\pi(N)(a_n-a_{n+1}).
\end{equation}
\end{lemm}

En effet,
\begin{align*}
\sum_{\substack{p\in\mathcal{P}\\ p\leq N}}a_p &= \sum_{k=1}^N\big(\pi(k)-\pi(k-1)\big)a_k\\
&= \pi(N)a_n+\sum_{k=1}^{N-1}\pi(k)a_k-\sum_{k=2}^N\pi(k-1)a_k.
\end{align*}
En appliquant l'identité \eqref{eq:4.1.3} à $a_n=\log n$, on obtient :
\begin{equation}\label{eq:4.1.4}
\sum_{\substack{p\in\mathcal{P}\\ p\leq N}}\log p=\pi(N)\log N-\sum_{n=1}^{N-1}\pi(n)\log\left(1+\frac1n\right).
\end{equation}
D'après le théorème des nombres premiers 1.0.2,
\begin{equation}\label{eq:4.1.5}
\pi(N)\log N\sim N\text{ lorsque }N\to+\infty
\end{equation}
tandis que
\[
\pi(n)\log\left(1+\frac1n\right)\sim\frac{1}{\log n}\text{ lorsque }n\to+\infty.
\]
Comme
\[
\sum_{n=2}^{N-1}\frac{1}{\log n}\sim\frac{N}{\log N}=o(N)\text{ lorsque }n\to+\infty),
\]
on a donc
\begin{equation}\label{eq:4.1.6}
\sum_{n=1}^{N-1}\pi(n)\log\left(1+\frac1n\right)=o(N).
\end{equation}
\noindent L'équivalent \eqref{eq:4.1.2} découle de \eqref{eq:4.1.4}, \eqref{eq:4.1.5} et \eqref{eq:4.1.6}.

\item \eqref{eq:4.1.6} $\Rightarrow$ \eqref{eq:1.0.2}.
Compte tenu de \eqref{eq:4.1.2}, la majoration
\[
\sum_{\substack{p\in\mathcal{P}\\ p\leq N}}\log p\leq\pi(x)\log x
\]
montre que
\[
\liminf_{x\to+\infty}\pi(x)\frac{\log x}{x}\geq1.
\]
Par ailleurs, pour tout $\eta\in]0;1[$ et tout $x\in\R_+^*$, il vient :
\[
\sum_{\substack{p\in\mathcal{P}\\ p\leq x^\eta}}\log p\leq\pi(x^\eta)\leq x^\eta\log x,
\]
ce qui implique, d'après \eqref{eq:4.1.2}, que pour tout $\eta\in ]0;1[$ :
\begin{equation}\label{eq:4.1.7}
\sum_{\substack{p\in\mathcal{P}\\ x^\eta<p\leq x}}\log p\sim x\text{ lorsque }x\to+\infty.
\end{equation}
Par ailleurs, pour tout $x\in\R_+^*$,
\begin{equation}\label{eq:4.1.8}
\sum_{\substack{p\in\mathcal{P}\\ x^\eta<p\leq x}}\log p\geq[\pi(x)-\pi(x^\eta)]\log x^\eta\geq\eta\pi(x)\log x-\eta x^\eta\log x.
\end{equation}
Il découle alors de \eqref{eq:4.1.7} et \eqref{eq:4.1.8}
\[
\eta\limsup_{x\to+\infty}\pi(x)\frac{\log x}{x}\leq1,
\]
puis, comme $\eta\in]0;1[$ est arbitraire :
\[
\limsup_{x\to+\infty}\pi(x)\frac{\log x}{x}\leq1
\]
ce qui achève la démonstration.
\end{enumerate}
\end{proof}

Dans la continuation immédiate des travaux de Thébychev, Mertens établit en 1874 des formules asymptotiques pour des sommes portant sur des nombres premiers.
Citons l'une d'entre elles :

\begin{theo}[Mertens] Lorsque $x \rightarrow +\infty$, on a :
\[
\sum_{\substack{p\in\mathcal{P}\\p\leq x}}\frac{\log p}{p}=\log x+O(1).
\]
\end{theo}

\begin{proof}
On a déjà montré dans la section 3.3 que
\[
\sum_{d\leq x}\frac{\Lambda(d)}{d}=\log x+O(1).
\]
Or, on a
\begin{align*}
\sum_{\substack{p\in\mathcal{P}\\ p\leq x}}\frac{\log p}{p}-\sum_{d\leq x}\frac{\Lambda(d)}{d} &= \sum_{\substack{ p\in\mathcal{P}\\ p\leq x,p^\nu\geq 2}}\frac{\log p}{p^\nu}\\
&\leq \sum_{p\in\mathcal{P}}\frac{\log p}{p(p-1)} \ll1,
\end{align*}
d'où le résultat.
\end{proof}

On remarque enfin que pour tout $\lambda\in]1;+\infty[$, le théorème des nombres premiers montre que, lorsque $x\to+\infty$, on a
\[
\pi(\lambda x)\sim\frac{\lambda x}{\log x}
\]
et donc
\[
\pi(\lambda x)-\pi(x)\sim\frac{(\lambda-1)}{\log x}.
\]
On en déduit l'énoncé suivant :

\begin{prop}
Pour tout $\lambda\in]1;+\infty[$, il existe $x(\lambda)\in\R_+$ tel que, pour tout $x\geq x(\lambda)$, l'intervalle $]x;\lambda x]$ contienne un nombre premier.
\end{prop}

\section{Zéros de $\zeta$, singularités de $\zeta_{\mathcal{P}}$ et distribution des nombres premiers}
On déduit immédiatement de la proposition 2.2.4 et du corollaire 2.2.5 le résultat suivant.

\begin{prop}
Les assertion suivantes sont équivalentes  :
\begin{enumerate}[(i)]
\item pour tout $t\in\R^*$, la fonction $\zeta_{\mathcal{P}}$ admet un prolongement holomorphe au voisinage de $1+it$ ;
\item la fonction $\zeta$ de Riemann ne possède pas de zéro sur la droite $1+i\R$ ;
\item pour tout $t\in\R^*$,
\end{enumerate}
\[
\zeta_{\mathcal{P}}(1+\sigma+it)=o(\log{\sigma}^{-1})
\]
lorsque $\sigma>0$ tend vers $0$ ;
\item la fonction $t\mapsto\zeta_{\mathcal{P}}(1+it)$ est de classe $\mathcal{C}^\infty$ sur $\R^*$.
\end{prop}

Comme mentionné dans l'introduction, les démonstrations originales du théorème des nombres premiers reposaient de façon cruciale sur le fait que ces conditions sont satisfaites, et il en va de même de toutes ses démonstrations fondées sur le développement en produit eulérien et les propriétés analytiques de $\zeta$.
Nous allons montrer qu'inversement ces conditions découlent du théorème des nombres premiers, ce qui mettra davantage en lumière le lien étroit qu'elles possèdent avec ce dernier et rendra moins surprenante la stratégie de démonstration.\\

Commençons par établir un résultat élémentaire sur les séries de Dirichlet.

\begin{lemm}
Soit $\sum_{n=1}^{+\infty}a_nn^{-s}$ une série de Dirichlet, de coefficients $a_n\in \R_+^*$, dont l'abscisse de convergence absolue $\sigma_0$ appartient à $\R$ et qui diverge en $\sigma_0$.
Soit en outre $\sum_{n=1}^{+\infty} b_nn^{-s}$ une série de Dirichlet, à coefficients complexes, telle que
\begin{equation}\label{eq:4.2.1}
\lim_{n \rightarrow +\infty} \frac{b_n}{a_n} = 1.
\end{equation}

On a alors, lorsque $\Re(s) > \sigma_0$ et que $\Re(s)$ tend vers
$\sigma_0$ :

$$\left|\, \sum_{n=1}^{\infty} b_n n^{-s} - \sum_{n=1}^{\infty} a_n n^{-s}\right| = o\left(\sum_{n=1}^{\infty} a_n n^{\Re(s)}\right).$$
\end{lemm}

On observe que l'hypothèse \eqref{eq:4.2.1} entraîne que l'abscisse de convergence absolue de la série $\sum_{n=1}^{+\infty}b_nn^{-s}$ est $\leq\sigma_0$.

\begin{proof}
Soit $s\in\R_+^*$, il existe $n\in\N$ tel que, pour tout $n>N$,
\[
|b_n - a_n|\leq\varepsilon a_n.
\]
On a alors, pour tout $s$ tel que $\re(s)>\sigma_0$ :
\[
\left|\sum_{n=1}^{+\infty}b_nn^{-s}-\sum_{n=1}^{+\infty}a_nn^{-s}\right|\leq\sum_{n=1}^{+\infty}(|a_n|+|b_n|)n^{-\re(s)}+\varepsilon\sum_{n=N+1}^{+\infty}a_nn^{-\re(s)}.
\]
Comme
\[
\sum_{n=1}^{+\infty}a_nn^{-\sigma_0}=+\infty,
\]
on a :
\[
\lim_{\sigma\to\sigma_0^+}\sum_{n=1}^{+\infty}a_nn^{-\sigma}=+\infty
\]
et donc
\[
\lim_{\sigma\to\sigma_0^+}\left(\sum_{n=1}^{+\infty}a_nn^{-\sigma}\right)^{-1}\sum_{n=1}^N(|a_n|+|b_n|)n^{- \sigma}=0.
\]
Par conséquent, il existe $\delta>0$ tel que
\[
0<\re(s)-\sigma_0<\delta\Rightarrow\left|\sum_{n=1}^{+\infty}b_nn^{-s}-\sum_{n=1}^{+\infty}a_nn^{-s}\right|\leq2\varepsilon\sum_{n=1}^{+\infty}a_nn^{-\re(s)}.
\]
Ceci achève la démonstration.
\end{proof}

D'après la formule de sommation \eqref{eq:4.1.3}, on a lorsque $\re(s)>1$,
\begin{align*}
\zeta_{\mathcal{P}}(s) &= \sum_{p\in\mathcal{P}}p^{-s}=\sum_{n=1}^{+\infty}\pi(n)[n^{-s}-(n+1)^{-s}]\\
&= \sum_{n=1}^{+\infty}\pi(n)n^{-s}\left[1-\left(1+\frac1n\right)^{-s}\right].
\end{align*}
On pose, pour $(s,n)\in\C\times\N^*$,
\[
1-\left(1+\frac1n\right)^{-s}=\frac{s}{n}+R(n,s).
\]
On vérifie que lorsque $s$ varie dans un compact $K$ de $\C$, on a
\begin{equation}\label{eq:4.2.2}
|R(n,s)|\leq C_Kn^{-2},
\end{equation}
où $C_K$ désigne une constante ne dépendant que de $K$.
Il vient alors, si $\re(s)>1$ :
\begin{equation}\label{eq:4.2.3}
\zeta_{\mathcal{P}}(s)=s\sum_{n=1}^{+\infty}\pi(n)n^{-s-1}+\sum_{n=1}^{+\infty}\pi(n)n^{-s}R(n,s).
\end{equation}
D'après \eqref{eq:4.2.2}, la seconde somme converge pour tout $s$ tel que $\re(s)>0$, normalement sur tout compact de $\{\re(s)>0\}$ (car on a $\pi(n)\leq n$), et définit donc une fonction holomorphe sur ce demi-plan.

Pour étudier la première somme, on introduit la série de Dirichlet
\[
Z(s):=\sum_{n=2}^{+\infty}\frac{1}{\log n}\frac{1}{n^s}.
\]
On vérifie que son abscisse de convergence absolue vaut $1$ et que, si $\re(s)>1$,
\[
Z'(s)=\sum_{n=2}^{+\infty}\frac{1}{n^s}=1-\zeta(s).
\]
Il en découle que, pour tout $t\in\R^*$, $Z$ admet un prolongement holomorphe sur un voisinage de $1+it$.
Comme $\zeta$ possède un pôle simple en $1$, avec comme résidu $1$, il vient de plus :
\[
Z(1+\sigma)=\log\sigma^{-1}+O(1)
\]
lorsque $\sigma>0$ tend vers $0$.\\

Par ailleurs, le théorème des nombres premiers permet d'appliquer le lemme 4.1.2 aux séries de Dirichlet $\sum_{n=1}^{+\infty}\pi(n)n^{-s}$ et $Z(s)$.
On obtient ainsi que lorsque $\re(s)$ tend vers $1^+$ :
\[
\left|\,\sum_{n=1}^{+\infty}\frac{\pi(n)}{n}\frac{1}{n^s}-Z(s)\right|=o(\log(\re(s) - 1)^{-1}),
\]
\noindent puis que, pour tout $t\in\R_+,$
\[
\sum_{n=1}^{+\infty}\frac{\pi(n)}{n}\frac{1}{n^{1+\sigma+it}}=o(\log\sigma^{-1})
\]
lorsque $\sigma>0$ tend vers $0$.\\

Compte tenu de \eqref{eq:4.2.3} et de l'holomorphie sur $\re(s)>0$ de la seconde somme dans \eqref{eq:4.2.3}, il en découle que la condition (iii) de la proposition 4.1.1. est satisfaite.
