% Olivier Rodriguez - février 2014
% Document distribué sous licence CC-BY-SA 3.0 FR
% http://creativecommons.org/licenses/by-sa/3.0/fr/

\chapter{Introduction}
L'histoire de l'étude de la distribution des nombres premiers commence il y a près de vingt-trois siècles par un premier résultat dû à Euclide.

\begin{theo}[Euclide]
Il existe une infinité de nombres premiers.
\end{theo}

\begin{proof}
Soient $p_1,p_2,\dots,p_n$ des nombres premiers, posons
\[
P_n=\prod_{i=1}^np_i\text{ et } N_n=P_n+1
\]
et soit $p_{n+1}$ le plus petit facteur premier de la décomposition de $N_n$.
Alors $p_{n+1}$ est un nombre premier $\leq N_n$ et distinct des $p_1,p_2,\ldots,p_n.$
Ainsi $(p_k)_{k>1}$ est une suite infinie de nombres premiers distincts.
\end{proof}

Il existe plusieurs autres démonstrations de ce théorème, dues notamment à Euler (1737), Polya (1920) et Erdös (1938).
Aucune n'est plus simple que celle d'Euclide.
Celle d'Euler\footnote{Euler établit le théorème d'Euclide sous une forme forte :
\[
\sum_{\substack{p\in \mathcal{P}\\ p\leq x}}\frac1p\geq\log_2x-\log2,\quad\forall x\geq 2,
\]
autrement dit : la série des inverses des nombres
premiers diverge.} a le mérite de montrer, pour la première fois, que l'Analyse permet de démontrer des résultats sur des nombres entiers.
Cette première incursion de l'Analyse dans l'Arithmétique se mua au XIXe siècle en une véritable invasion pour créer la branche des mathématiques que l'on appelle aujourd'hui la Théorie Analytique des Nombres.\\

Par ailleurs, les mathématiciens s'intéressent depuis longtemps
aux questions suivantes :
\begin{itemize}
\item La répartition des nombres premiers dans la suite des naturels
est-elle régulière ?
\item Présente-t-elle des particularités intéressantes ?
\end{itemize}
Ils sont arrivés à la conclusion que cette répartition était plutôt anarchique et que globalement, la proportion des nombres premiers va en décroissant.\\

Soit $\mathcal{P}$ l'ensemble des nombres premiers,
et pour tout $x \in \Bbb{R}$, soit
\[
\pi(x)=\#\{p\in\mathcal{P}:p\leq x\}=\sum_{\substack{p\in\mathcal{P}\\ p\leq x}}1
\]
le nombre de nombres premiers inférieurs ou égaux à $x$.
Gauss (1792) puis Legendre (1798, 1808) conjecturent que l'on a, lorsque $x\to+\infty$ :
\[
\pi(x)\sim\frac{x}{\log x}.
\]
En 1852, Thébychev établit une forme faible de cette conjecture par des moyens élémentaires :
\[
\exists A, B\in\Bbb{R}, 0<A<1<B:\quad\forall x\geq2,\quad A\frac{x}{\log x}<\pi(x)<B\frac{x}{\log x}.
\]
Numériquement, le résultat constitue un argument très convaincant en faveur de la conjecture de Gauss-Legendre : pour $x$ suffisamment grand, on peut choisir $A=0,921$ et $B=1,106$.\\

Dès 1737, Euler avait fait usage de la fonction $\zeta$, comme fonction de la variable réelle, pour étudier la suite des nombres premiers.
On rappelle que cette fonction est définie sur le demi-plan ouvert $\{\re(s)>1\}$ par l'expression
\begin{equation}\label{eq:1.0.1}
\zeta(s)=\sum_{n=1}^{+\infty}\frac{1}{n^s}.
\end{equation}
Le lien entre les propriétés analytiques de la fonction $\zeta$ et la répartition des nombres premiers est établi par l'expression de $\zeta(s)$ comme \emph{produit eulérien} :
\[
\zeta(s)=\prod_{p\in\mathcal{P}}{\left(1-\frac{1}{p^s}\right)}^{-1},\quad\forall s\in\{\re(s)>1\}.
\]
Cette identité montre en outre que $\zeta(s)$ ne s'annule pas sur le demi-plan $\{\re(s)>1\}$.

Riemann exploite l'idée géniale de prolonger la fonction $\zeta$ en une fonction de la variable complexe, ce qui permet de donner un sens à $\zeta(s)$ même hors du domaine de convergence de la
série.
Il établit en 1859 l'existence d'un prolongement méromorphe à tout le plan complexe de la fonction $\zeta$ dont l'étude se prête remarquablement bien à la déduction de renseignements concernant $\pi(x)$.
Dans son article \emph{«Über die Anzahl der Primzahlen unter einer gegebenen Grösse»}\footnote{\emph{«Sur le nombre de nombres premiers inférieurs à une grandeur donnée»}.}, Riemann met en évidence -- quoique de manière largement conjecturale -- le lien étroit entre la distribution des nombres premiers et les zéros de $\zeta$ dans le plan complexe.\\

Enfin, en 1896, le Français Jacques Hadamard et le Belge Charles de La Vallée-Poussin, s'appuyant l'un et l'autre sur les travaux de Riemann, démontrent indépendamment la conjecture de Gauss-Legendre :

\begin{theo}[Théorème des Nombres Premiers]
Lorsque $x \to+\infty$, on a :
\begin{equation}\label{eq:1.0.2}
\pi(x)\sim\frac{x}{\log(x)}.
\end{equation}
\end{theo}

Un point essentiel de la démonstration de Hadamard et de La Vallée-Poussin est que le prolongement méromorphe de la fonction $\zeta$ de Riemann admet $1$ comme unique pôle et \emph{ne s'annule pas} sur la droite $\{\re(s)=1\}$.
C'est cet argument capital qui sera établi et exploité dans la première preuve que nous présentons au chapitre suivant.
